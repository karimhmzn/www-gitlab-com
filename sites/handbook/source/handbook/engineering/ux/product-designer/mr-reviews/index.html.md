---
layout: handbook-page-toc
title: Merge Request Reviews
description: "Guidelines for Product Designers when doing merge request reviews."
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The [Product Design merge request (MR) review volume](/handbook/engineering/ux/performance-indicators/#product-design-mr-review-volume) is tracked as a Key Performance Indicator (KPI) of the UX department. 

## Requirement

**Almost all merge requests (MRs) change the UX in one way or another, but Product Designers are only required to review and approve MRs that include _user-facing changes_**. Per the [approval guidelines](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines), “user-facing changes include both visual changes (regardless of how minor) and changes to the rendered DOM that impact how a screen reader may announce the content.”

MRs with only backend changes sometimes affect the UX (for example, performance changes, sorting of lists, etc.), but you _are not required_ to review them unless they fall under the definition of “user-facing changes”.

To help triage, be aware of all MRs in your stage group and ask engineers about which MRs could affect the UX and how. Product Designers often give constructive feedback on any kind of MR, including MRs that _seem_ to not affect the UX, so use your best judgement when deciding which MRs you should review.

Product Designers should approve MRs, unless they introduce [bugs of severity 1 or 2](/handbook/engineering/quality/issue-triage/#severity). In these cases, MRs should not be approved and merged until those bugs are fixed. MRs with bugs of severity 3 and 4 can be merged, but we encourage authors to fix them before merging. To address any outstanding UX bugs, the Product Designer should create follow-up issues and label them as UX Debt.

## Reviewing

- Follow the [Code Review guidelines](https://docs.gitlab.com/ee/development/code_review.html). Exceptions to those guidelines are noted below.
- UX review requests in [MRs are high priority](/handbook/engineering/ux/product-designer/#priorities). Respond to them in accordance with our [first-response Service-Level Objective](/handbook/engineering/workflow/code-review/#first-response-slo), not only when the request comes from within your stage group, but also when it's a community contribution or another stage group asks for your quick input.
- Test locally, and do not rely on screenshots. [Need help using GDK or Gitpod to test locally?](/handbook/engineering/ux/how-we-work/index.html#using-gitlab-development-kit-gdk-and-gitpod)
- Be thorough. There should be as little back and forth as possible.
- If you are asked to review an MR for an issue you were not assigned to, remind the author who the assigned designer is and assign to original designer for review.
- When reviewing an MR, please use the following order of importance:
    - Functionality first: Does it work? Are there [accessiblity](https://design.gitlab.com/accessibility/best-practices) issues?
    - Edge cases: Are there any unexpected edge cases?
    - Visual consistency: Does it conform to our Design System and workflows in other product areas?
- Remember to stick to the issue. Create issues for further updates to avoid scope creep.
- During an MR Review, if you identify changes that need to be made:
  - **UX Debt:** If the change stems from a team decision to intentionally deviate from the agreed-upon UX vision or MVC due to timelines or technical feasibility challenges, approve the MR, create follow-up issues to address the experience gaps, and apply the `UX debt` label to the issues. 
  - **Other changes:** If you realize that the solution is not ideal and you want to propose changes, approve the MR and create a regular issue.
- Once you have completed the review process, use the **Approve** button to indicate you have completed your review. You can unassign yourself from the MR.

Product Designers should feel empowered to adapt the process to fit their situation, as long as they feel confident that UI changes are getting sufficient attention to avoid inflicting UX bugs or UX pain for customers. Some common scenarios include:

- Finding a UX problem but the team decides to merge the code anyway. When this happens, create a new issue to fix the problem and label it with `UX Debt` per this section on [UX labels](/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels).
- Doing visual reviews on staging instead of (or in addition to) locally. Examples are larger changes that span more than one MR or workflows that don’t test well locally. In this case, product designers can work with the frontend team to figure out the right timing for the review. In certain cases, it can be fine to merge MRs prior to a full visual review, as long as the functionality remains behind a feature flag and a plan is in place for the visual review to occur in staging. Keep in mind that this can result in more new issues and MRs, if the product designer finds things that need to be fixed.
