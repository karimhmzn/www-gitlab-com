---
layout: handbook-page-toc
title: "Marketing Operations"
description: "Marketing Operations (MktgOps) supports the entire Marketing team to streamline processes and manage related tools. Due to those tools, we often support other teams at GitLab as well."
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## <i class="far fa-newspaper" id="biz-tech-icons"></i> Charter

Marketing Operations (MktgOps) supports the marketing organization to streamline processes and manage related tools. We work closely with multiple teams to ensure information between systems is seamless, data is as accurate as possible, and terminology is consistent in respective systems. Our team's primary functions are:

- Project management of marketing technology stack
- Streamline and standardize processes related to tools 
- Data cleanliness and accuracy
- Best practices and strategy on marketing technology
- Continuous improvement of marketing systems
- System integrations and data flow
- Evaluate new marketing technology
- Support procurement of new marketing technology

## <i class="fas fa-users" id="biz-tech-icons"></i> Meet the Team

Our team is structured as business partners to the rest of marketing. See focus next to the names below:

| Person | Role | Focus |
| ------ | ------ | ------ |
| [Amy Waller](https://gitlab.com/amy.waller)* | [Senior Marketing Operations Manager](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#senior-marketing-operations-manager) | Campaigns |
| [Beth Peterson](https://gitlab.com/bethpeterson)* | [Senior Marketing Operations Manager](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#senior-marketing-operations-manager) | SDRs |
| [Gillian Murphy](https://gitlab.com/gillmurphy) | [Marketing Operations Manager](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#marketing-operations-manager-intermediate) | Content, Localization, and SDRs |
| [Jameson Burton](https://gitlab.com/jburton) | [Marketing Operations Associate](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#associate-marketing-operations-manager) | Digital, Operations |
| [Robert Rosu](https://gitlab.com/RobRosu) | [Marketing Operations Manager](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#marketing-operations-manager-intermediate) | Data |
| [Mihai Conteanu](https://gitlab.com/MihaiConteanu) | [Marketing Operations Manager](https://about.gitlab.com/job-families/marketing/marketing-operations-manager/#marketing-operations-manager-intermediate) | Content, Campaigns |

_*indicates business partner_

## <i class="far fa-handshake" id="biz-tech-icons"></i> Teams We Work Closely With

<div class="flex-row" markdown="0" style="height:80px">
    <a href="/handbook/marketing/revenue-marketing/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Revenue Marketing</a>
    <a href="/handbook/marketing/corporate-marketing" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Corporate Marketing</a>
    <a href="/handbook/marketing/community-relations/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Community Relations</a>
    <a href="/handbook/marketing/strategic-marketing/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Strategic Marketing</a>
    <a href="/handbook/marketing/inbound-marketing/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Inbound Marketing</a>
</div>   

<div class="flex-row" markdown="0" style="height:80px">
    <a href="/handbook/sales/field-operations/sales-operations" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Operations</a>
    <a href="/handbook/finance/procurement/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Procurement</a>
    <a href="/handbook/legal/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Legal</a>
    <a href="/handbook/business-ops/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Operations</a>
    <a href="/handbook/engineering/infrastructure/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Infrastructure</a>
</div>  

<div class="flex-row" markdown="0" style="height:80px">
    <a href="/handbook/support/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Support</a>
</div>

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to Communicate with Us

**Slack channels**

We do not use or create tool-specific Slack channels (e.g. `#marketo`).

- [#mktgops](https://gitlab.slack.com/archives/mktgops) - We use this channel for general marketing operations support, weekly marketing operations team standup updates, and key system status updates. We attempt to [avoid direct messages](https://about.gitlab.com/handbook/communication/#avoid-direct-messages) where possible as it discourages collaboration. 
- [#hbupdate-mktgops](https://gitlab.slack.com/archives/mktgops) - This channel is used to automatically post new [handbook updates](#handbook-updates) that have been merged. 

**Emergency Comms and Pager Duty**

If an emergency communication needs to be send out, Marketing Ops will need to assist. Follow directions on this [page](/handbook/marketing/emergency-response/) to initiate the emergency response. You can also follow the [security incident communication plan](/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html) for security related issues.

## <i class="far fa-life-ring" id="biz-tech-icons"></i> How to Get Help

**Important**: Before submitting an issue that may contain [Personally Identifable Information (PII) data](https://about.gitlab.com/handbook/support/workflows/pii_removal_requests.html#overview) (including screenshots), please ensure the issue is marked confidential. You can use [quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html#issues-merge-requests-and-epics) to accomplish this in the issue description priort to submitting.

<div class="flex-row" markdown="0">
  <div>
    <a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=bug_request" class="btn btn-purple" style="width:170px;margin:5px;">Report a bug</a>
    <a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
    <a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=process_change_request" class="btn btn-purple" style="width:170px;margin:5px;">Process change request</a>
    <a href="/handbook/marketing/emergency-response" class="btn btn-purple" style="width:170px;margin:5px;">Emergency Comms</a>
  </div>
</div>

## <i class="fas fa-tasks" id="biz-tech-icons"></i> How We Work

**Our Motto:** _If it isn't an Issue, it isn't OUR issue!_

<div class="flex-row" markdown="0">
  <div>
    <a href="https://gitlab.com/gitlab-com/marketing/marketing-operations" class="btn btn-purple" style="width:170px;margin:5px;">Marketing Operations project</a>
  </div>
</div>

### Issue Boards

1. [MktgOps Priority (issues by priority)](https://gitlab.com/groups/gitlab-com/-/boards/2626681)
1. [MktgOps Status (issues by status)](https://gitlab.com/groups/gitlab-com/-/boards/2629676)
1. [MktgOps Team (issues by team member)](https://gitlab.com/groups/gitlab-com/-/boards/2629685)

### Issues

The MktgOps team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~MktgOps::0 - To Be Triaged label anywhere within the GitLab repo. Prior to opening a new issue, feel free to reach out to your business partner to see if there is already a related issue that you can add your comments to. If you have a bug, error or discrepancy you'd like the team to help and investigate, please use the [bug-request template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/bug_request.md).

With [Agile Delivery](https://about.gitlab.com/solutions/agile-delivery/) being one of the solutions that GitLab (as a product) addresses, the Marketing Operations team aims to follow many of the agile methodologies. 

Please do not reopen issues that have been closed in a previous milestone. If you find that you have additional questions about a closed issue, comment in the issue and ping the marketing ops DRI who worked the issue. The DRI within our team will determine whether an issue needs to be reopened and pulled into a current milestone. 

### Weights

To help prioritize issues and scope work in our milestones, we've adopted GitLab [issue weights](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html#issue-weight) to our issues. By adding a weight to our issues, our team will have a better idea of how much time, value or complexity a given issue has or costs. For the numbering formula, we are using the traditional Fibonacci sequence: 1, 2, 3, 5, 8, 13, 21. Anything marked `21` or above may be promoted to an epic. 

### Health Status

To indicate status of issues related to [OKRs](https://about.gitlab.com/company/okrs/), we've adopted [GitLab health status](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#health-status). By adding health status to issues tied to OKRs, you can quickly see whether an OKR-related epic is on track to complete within a quarter based on the health status of the issues tied to it.

### Epics

If an issue includes a weight of 21 or more, that issue may be promoted to an epic in order to properly scope the work across multiple issues. Epics will also be used by our team if it relates to an OKR and requires multiple issues in scope to complete the work. Tool implementations also often are tracked within epics. 

### Labeling

We use labels for three purposes:

1. Categorize the tool or area that is affected
1. Show priority
1. Identify the stage they are in

<details>
<summary markdown='span'>
Categories
</summary>

- `MktgOps - FYI`: Issue is not directly related to operations, no action items for MktgOps but need to be aware of the issue
- `MktgOps - List Import`: Used for list imports of any kind - event or general/ad hoc (do not also use To Be Triaged scoped label)
- `Marketo`, `Bizible`, `Cookiebot`, `Demandbase`, `Drift`, `GDPR`, `LeanData`, `LinkedIn Sales Navigator`, `Outreach-io`, `PathFactory`, `Periscope`, `Terminus`, `ZoomInfo`, `Smartling`, `Vimeo`, `OneTrust`, `Typeform`, `iconik`, `Backblaze`: used to highlight one of our tech stack tools
- `MktgOps - bug`: A bug issue to be addressed or identified by MktgOps
- `MktgOps - changelog`: Used to track issues or epics that would need to be logged in the marketing changelog to track major changes across marketing
- `MktgOps-Future Feature`: Something to consider for a future project as time allows. Timeframe: As time allows

</details>

<details>
<summary markdown='span'>
Priorities
</summary>

- `MktgOps-Priority::Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by MktgOps leadership. This category will be limited because not everything can be a priority. Timeframe: Immediate action needed.
- `MktgOps-Priority:: High Priority`: Issue has a specific action item for MktgOps to be completed as it is an OKR. Timeframe: Within weeks
- `MktgOps-Priority::Med Priority`: Issue is a feature to help the team. Timeframe: Within months
- `MktgOps-Priority::Low Priority`: Issue has a specific action item for MktgOps that would be helpful, but can be pushed for other issues. Timeframe: Within months.

</details>

<details>
<summary markdown='span'>
Urgency
</summary>

- `MktgOps-Urgency::P0`: System down, core business function down, or potential loss of mission critical data. Timeframe: Immediate action needed. Pull into active milestone.
- `MktgOps-Urgency::P1`: Major feature or workflow is not working. Timeframe: Action within days. Pull into active milestone.
- `MktgOps-Urgency::P2`: Requires attention, but normal workflow is not impacted or there is a workaround. Timeframe: Action within next milestone.
- `MktgOps-Urgency::P3`: Requires attention, but normal workflow is not impacted. Minor, low urgency. Timeframe: Future action needed, not priority or urgent.
- `MktgOps-Urgency::P4`: Small bug, nice to have fixed, but not impacting workflow. Timeframe: Future action needed, not priority or urgent.

</details>

<details>
<summary markdown='span'>
Stage
</summary>

- `MktgOps::0 - To Be Triaged`: Issue initially created, used in templates, the starting point for any label that involves MktgOps (except for List Uploads); no real discussion on the issue; generally unassigned
- `MktgOps::1 - Planning`: Issues assigned to a MktgOps team member and are currently being planned but are not being actively worked on yet.
- `MktgOps::2 - Scoping`: Marketing Operations related issues that are currently being scoped and weighted
- `MktgOps::3 - On Deck`: Issues that have been scoped and will be added to an upcoming milestone.
- `MktgOps::4 - In Process`: Issues that are actively being worked on in the current two-week milestone.
- `MktgOps::5 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for User Acceptance Testing/review and approval by the Requester/Approver.
- `MktgOps::6 - On Hold`: Issue that is not within existing scope of MktgOps current focus, or another department as deprioritized. May be a precursor to being closed out. May also be on hold waiting for an internal team member or other task before completing. 
- `MktgOps::7 - Blocked`: Issue is blocked and no other actions can be taken by MktgOps. Waiting for someone else/another team to complete an action item before being able to proceed. May also be blocked due to external party such as a vendor to complete an action before closing.
- `MktgOps::8 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed.

</details>

### Milestones

The MktgOps team works in two week iterations which are tracked as milestones at the `GitLab.com` level. Each individual contributor (IC) is responsible for adding issues to the milestone that will be completed in the two-week time frame. If needed, the IC will separate the main issue into smaller pieces that are _workable_ segments of the larger request.

A milestone cannot be closed nor marked complete until the milestone's accompanying merge request has been merged. Within every milestone there is a `WIP` merge request with all commits being changes to our handbook. All team members contribute their changes to the milestone merge request. The merge request should be tagged with marketing operations labels and the current milestone.

<div class="flex-row" markdown="0">
  <div>
    <a href="https://gitlab.com/groups/gitlab-com/-/milestones?utf8=%E2%9C%93&search_title=mktgops&state=&sort=" class="btn btn-purple" style="width:170px;margin:5px;">View Milestones</a>
  </div>
</div>

### Handbook Updates

When making an update to a handbook page for `ABM`, `FMM`, `MOps`, or `SDR` handbook pages (or sub-pages), we have a Zapier workflow set up that will push the MR (upon merge) to the related Slack channel to ensure our teams are aware of any change that is made to the page. In order for the merged MR to show up in the respective Slack channel, you must add one of the following corresponding `labels` on the MR. Slack updates will also trigger for MktgOps MRs when created.

| Label you add | Slack channel the merged MR pushes to |
| ------------- | ------------------------------------- |
| `MktgOps - HB Update` | `#hbupdate-mktgops` `#mktgops` |
| `FMM-HB Update` | `#fieldmarketing-FYI` |
| `SDR-HB Update` | `#hbupdate-sdr` |
| `ABM-HB Update` | `#hbupdate-abm` |

#### Milestone MRs 

The marketing operations team uses 2 collective merge requests (1 per week in a milestone), known as our milestone MRs, to make multiple updates across our handbook, see high-level updates in 1 MR, and avoid conflicts with each other. Here is an [example](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71109). We list all major changes with our GitLab username in the description after a commit and link any relevant issues that the commit closes out. If you have an update for the marketing operations handbook, please feel free to use our milestone MR to make a commit and tag us for review to avoid conflicts.

### Marketing Ops Calendar

<iframe src="https://calendar.google.com/calendar/embed?src=c_u3gaaimqhrc3d753nbol3houkg%40group.calendar.google.com&ctz=America%2FLos_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

### Focus Fridays

The Marketing Operations team had started an experiment on 2020-04-20 to commit to no internal meetings one day of the week. Now the entire Marketing team has moved to [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays). Please try not to schedule meetings for team members on Fridays, so they can devote time for deep work in milestone-related issues. 

### Marketing Changelog

Periodically Marketing Operations and other teams through the marketing org make significant changes to our system and processes that affect overall tools, data and reporting or uncovers significant changes that affected reporting. As such we have a shared [changelog](https://docs.google.com/spreadsheets/d/1FHiKhQukMVfwKsBJDzyrsuzuw2bv97xQFhegvFXTeNQ/edit#gid=0). The MktgOps and Strategy/Perf teams update this document as needed as changes are made. If you are working on an issue or epic that will have a significant impact across marketing, add the label `MktgOps - changelog` so marketing oeprations can track changes across GitLab.


## <i class="far fa-folder-open" id="biz-tech-icons"></i> Important Resources

- [Marketing Metrics](/handbook/marketing/marketing-operations/marketing-metrics)
- [Marketing Owned Provisioning Instructions](/handbook/marketing/marketing-operations/marketing-owned-provisioning)

## <i class="fas fa-file-import" id="biz-tech-icons"></i> [List Imports](/handbook/marketing/marketing-operations/list-import)

## <i class="fas fa-toolbox" id="biz-tech-icons"></i> Tech Stack

For information regarding the tech stack at GitLab, please visit the [Tech Stack Applications page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) of the Business Operations handbook where we maintain a comprehensive table of the tools used across the company. Below are tools that are primarily owned and managed by marketing operations.

<details>
<summary markdown='span'>
Integrated with Salesforce
</summary>

- [Bizible](/handbook/marketing/marketing-operations/bizible/)
- [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/)
- [Drift](/handbook/marketing/marketing-operations/drift)
- [LeanData](/handbook/marketing/marketing-operations/leandata)
- LinkedIn Sales Navigator
- [Marketo](/handbook/marketing/marketing-operations/marketo)
- [Outreach.io](/handbook/marketing/marketing-operations/outreach)
- [PathFactory](/handbook/marketing/marketing-operations/pathfactory)
- [Terminus Email Experiences](/handbook/marketing/marketing-operations/terminus-email-experiences)
- [ZoomInfo](/handbook/marketing/marketing-operations/zoominfo/)

</details>

<details>
<summary markdown='span'>
Other tools directly used by Marketing and maintained by Marketing Operations
</summary>

- [Bizzabo](/handbook/marketing/marketing-operations/bizzabo)
- [Backblaze](/handbook/marketing/marketing-operations/backblaze)
- Disqus
- Eventbrite
- Figma
- Frame.io
- Google Adwords
- Google Analytics
- Google Search Console
- Google Tag Manager
- Hotjar
- [iconik](/handbook/marketing/marketing-operations/iconik)
- Keyhole
- LaunchDarkly
- [LogRocket](/handbook/marketing/marketing-operations/logrocket)
- [Litmus](/handbook/marketing/marketing-operations/litmus)
- MailChimp
- [OneTrust](/handbook/marketing/marketing-operations/onetrust)
- [Rev](/handbook/marketing/marketing-operations/rev)
- SEMrush
- Sitebulb
- [Smartling](/handbook/marketing/marketing-operations/smartling)
- Sprout Social
- Swiftype
- Survey Monkey
- [Typeform](/handbook/marketing/marketing-operations/typeform/)
- [Tweetdeck](/handbook/marketing/marketing-operations/tweetdeck/)
- [YouTube](/handbook/marketing/marketing-operations/youtube/)
- [Vimeo](/handbook/marketing/marketing-operations/vimeo/)
- [Zapier](/handbook/marketing/marketing-operations/zapier/)

</details>

### Requesting access to an existing tool

To request access to an existing tool in the stack, [please follow the access request process](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/) as outlined in the business operations handbook.

If you are working with a contractor or consultant that requires access to a tool in our stack, [please follow the professional services access request process](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/#-step-7--create-professional-services-access-request-optional) as outlined in the procurement handbook.

### Requesting a new tool

If you are interested in or would like to request a new tool be added to the tech stack, [please submit an issue using the tools eval issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=tools_eval) in the Marketing Operations repository. Marketing Operations should be included in new tool evaluations to account for system integrations, budget, etc. Any new tools desired after the budget is set will be handled by transferring budget from the other department to Marketing Operations.

### Tool usage and tool access

For budgetary reasons, Marketing Operations will be performing quarterly, and for some tools bi-quaterly, audits on the user activity of marketing tools. If a team member has not been actively taking advantage of a tool for 45 days or more, they will have access to that tool revoked with 5 business days of notification via email. Activity will be determined by user reports pulled by the tools' admins. These reports can be found by viewing issues from the Marketing Ops project with the issue label `Mktg Tool Audit`. The reports will utilize the audit issue template from the Marketing Ops project. To regain access to revoked tools, the team member will need to submit a new access request and follow standard access request procedures. However, user seats will be on a first-come-first-serve basis unless it is determined additional seats should be purchased.

- Tools included under the quarterly guidelines
  - Outreach
  - ZoomInfo
  - Linkedin Sales Navigator
  - PathFactory
- Tools included under the bi-quaterly guidelines, Q1 and Q3
  - Terminus (formerly Sigstr)
  - Drift
  - Marketo
  - Litmus 

## <i class="fas fa-receipt" id="biz-tech-icons"></i> Marketing Expense Tracking

| **GL Code** | **Account Name** | Purpose |
| ----------- | -----------------| ------- |
| 6060 | Software Subscriptions | All software subscriptions |
| 6100 | Marketing  | Reserved for Marketing GL accounts |
| 6110 | Marketing Site | All agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising | All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events | All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6135 | Swag | Any swag related expense |
| 6140 | Email | All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand | All PR, AR, content, swag and branding costs |
| 6160 | Prospecting | Not used - All costs related to prospecting efforts |

### Invoice Approval

Marketing Operations approves any invoices that have not already been coded and approved through a Finance issue or that exceed the original cost estimate. We make use of Tipalti for this process. Team leads will confirm that services were performed or products were received also through Tipalti. Campaign tags are used to track costs related to events and campaigns.

## Lead Scoring, Lead Lifecycle, and MQL Criteria

A Marketing Qualified Lead is a lead that has reached a certain threshold, we have determined to be 100 points accumulated, based on demographic/firmographic and/or behavioral information. The `Person Score` is comprised of various actions and/or profile data that are weighted with positive or negative point values. `Person Score` is one of the indicators used by LeanData to determine whether a lead needs to be assigned. Details about this process can be found on the [LeanData page](https://about.gitlab.com/handbook/marketing/marketing-operations/leandata/#lead-routing-workflow). You can find more details about the scoring model on the [Marketo Page](/handbook/marketing/marketing-operations/marketo/#scoring-model)

## Campaigns and Programs

Campaigns are used to track efforts of marketing tactics - field events, webcasts, content downloads. The campaign types align with how marketing tracks spend and align the way records are tracked across three of our core systems (Marketo, Salesforce and Bizible) for consistent tracking. Leveraging campaigns aligns our efforts across Marketing, Sales and Finance.

Go to the [Campaigns and Programs Page](/handbook/marketing/marketing-operations/campaigns-and-programs/#campaigns) to view all of the campaign types, their progression statuses and cost tracking. That page also includes directions for set up in Marketo and Salesforce.

## Email Management

Marketing Ops is responsible for maintaining the email marketing database. Go to the [Email Management Page](/handbook/marketing/marketing-operations/email-management) for policies and more detailed information.

## Initial Source

`Initial Source` is the first "known" touch attribution or when a website visitor becomes a known name in our database, once set it should never be changed or overwritten. For this reason Salesforce is set up so that you are unable to update the `Initial Source` field. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the `#mktgops` Slack channel. `Initial Source` in Marketo is named `Person Source`, and should only update when empty.

We use Source Buckets to group raw Sources into acquisition channels. These groups are: core, inbound, outbound, paid demand gen, purchased list, referral, virtual event, and web direct. When using the [TD - Marketing Metrics dashboard](https://app.periscopedata.com/app/gitlab/798262/TD---Marketing-Metrics) reports can be filterd by these source buckets.

The values listed below are the only values currently supported. If you attempt to upload or import leads or contacts into Salesforce without one of these initial sources you will encounter a validation rule error. If you think that there needs to be a new Initial Source added to this list and into Salesforce please [open an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new) with the marketing ops team. When a new initial source is added, the bucket must also be updated in a SFDC workflow to properly show in Sisense.

Status in the table below means:

- Active = can be selected from picklist
- Inactive = cannot be selected from picklist, but a record may exist with this source

| Source | Source Bucket | Definition and/or transition plan | Status* |
| ------ | ------------- | --------------------------------- | ------- |
| CE Download | core | Downloaded CE version of GitLab | Active |
| CE Usage Ping | core | Created from CE Usage Ping data | Active |
| CORE Check-Up | core |  | Active |
| Demo | inbound | Filled out form to watch demo of GitLab | Active |
| Education | inbound | Filled out form applying to the Educational license program | Active |
| Email Request | inbound | Used when an email was received through an alias (_will be deprecated_) | Active |
| Email Subscription | inbound | Subscribed to our opt-in list either in preference center or various email capture field on GitLab website | Active |
| Gated Content - General | inbound | Download an asset that does not fit into the other Gated Content categories | Active |
| Gated Content - eBook | inbound | Download a digital asset categorized as an eBook | Active |
| Gated Content - Report | inbound | Download a gated report | Active |
| Gated Content - Video | inbound | Watch a gated video asset | Active |
| Gated Content - Whitepaper | inbound | Download a white paper | Active |
| GitLab.com | inbound | Registered for GitLab.com account | Active |
| Newsletter | inbound |  | Active |
| OSS | inbound | Open Source Project records related to the OSS offer for free licensing | Active |
| Request - Contact | inbound | Filled out contact request form on GitLab website | Active |
| Request - Professional Services | inbound | Any type of request that comes in requesting to engage with our Professional Services team | Active |
| Security Newsletter | inbound | Signed up for security alerts | Active |
| Trial - Enterprise | trial | In-product or web request for self-hosted Enterprise license | Active |
| Trial - GitLab.com | trial | In-product SaaS trial request | Active |
| Web | inbound |  | Active |
| Web Chat | inbound | Engaged with us through website chat bot | Active |
| Consultancy Request | inbound |  | Active |
| Drift | inbound |  | Active |
| Request - Community | inbound |  | Active |
| Request - Public Sector | inbound |  | Active |
| Startup Application | inbound |  | Active |
| Other | Other |  | Active |
| AE Generated | outbound | Sourced by an Account Executive through networking or professional groups | Active |
| Clearbit | outbound |  | Active |
| Datanyze | outbound |  | Active |
| DiscoverOrg | outbound |  | Active |
| Leadware | outbound | Sourced by an SDR through networking or professional groups | Active |
| LinkedIn | outbound |  | Active |
| Prospecting | outbound |  | Active |
| Prospecting - General | outbound |  | Active |
| Prospecting - LeadIQ | outbound |  | Active |
| SDR Generated | outbound | Sourced by an SDR through networking or professional groups | Active |
| Zoominfo | outbound |  | Active |
| Advertisement | paid demand gen |  | Active |
| Conference | paid demand gen | Stopped by our booth or received through event sponsorship | Active |
| Field Event | paid demand gen | Paid events we do not own but are active participant (Meetups, Breakfasts, Roadshows) | Active |
| Owned Event | paid demand gen | Events that are created, owned, run by GitLab | Active |
| Promotion | paid demand gen |  | Active |
| Virtual Sponsorship | paid demand gen |  | Active |
| Purchased List | purchased list |  | Active |
| Employee Referral | referral |  | Active |
| Partner | referral | GitLab partner sourced name either through their own prospecting and/or events | Inactive |
| Channel Qualified Lead | referral| GitLab partner sourced, previously `partner`| Active|
| Word of Mouth | referral |  | Active |
| Event Partner | referral |  | Inactive |
| Existing Client | referral |  | Active |
| External Referral | referral |  | Active |
| Webcast | virtual event | Register for any online webcast (not incl `Demo`) | Active |
| Webinar | virtual event |  | Active |
| Web Direct | web direct | Created when purchase is made direct through the portal (check for duplicates & merge record if found) | Active |
| Investor | outbound | Sourced by our investors (i.e. - GV, Khosla, ICONIQ). The `Investor` value is coupled with the `Investor Name` custom field | Active |
| GitLab DataMart|core| Created by the GitLab Marketing Database data pump. Contains leads from various internal sources|Active|

### A Note About Trials

In Q1 FY22 the Demand Generation team began running paid campaigns to drive trial signups. Currently, we don't have the "trial" source bucket split out by paid vs. organic because we don't have Google Analytics tracking on the signup form. Fortunately, this will soon be changing!

Until then, if pulling metrics around source buckets for CAC calculations please use the [Demand Gen dashboard in Sisense](https://app.periscopedata.com/app/gitlab/793304/Demand-Gen-Dashboard) (you can filter by trials).


## Lead and Contact Statuses

The Lead & Contact objects in Salesforce have unified statuses with the following definitions. Lead . Also reference [Re-MQL workflows](/handbook/marketing/marketing-operations/marketo/#re-mql) for how to move from status to status.

| Status | Definition |
| ------ | ---------- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketing Qualified through systematic means |
| Accepted | Actively working to get in touch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | (Temporary, to be merged by RingLead) Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |

## Data Cleanliness and Accuracy Process

Marketing Operations has under its belt the responsibility for cleaning and enriching our database of leads/contacts with the most complete and up to date information. 

The cleaning part of this process is currently being done using our lead/contact deduplication tool, Ringlead. The enrichment part of the process is done using the data appending/enrichment tool, [Zoominfo](https://about.gitlab.com/handbook/marketing/marketing-operations/zoominfo/), our SSOT when it comes to account/lead/contact data. 

This cleaning & enrichment process has 4 main priorities:  

1. **Increase Email Deliverability** by implementing email Validation & verification at point of capture (POC) - Implemented with [NeverBounce](https://neverbounce.com/?fbclid=IwAR1bdezYBpqMH58zm24yg_RFGdeF4VCgBHVQCJKYtTyf-Iegd9gZ1_GhTII). 
2. **Enrich net new leads with Form Complete**  - Soon to be live on `Contact Us`, `Self-Managed Trials`, `SaaS Trials` forms. 
3. **Existing Database Enrichment** - Zoominfo has the possibility of enriching either via Marketo or SFDC. Our current process enriches the existing leads/contacts in our database via SFDC, scheduled enrich, while the new leads are currently enriched via a Marketo Webhook and Marketo Zoominfo integration. You can find more details on the [Zoominfo Handbook](https://about.gitlab.com/handbook/marketing/marketing-operations/zoominfo/). 
4. **Assure Data Cleanliness & Accuracy** - Lead & Contact Deduplication splits naturally in three main parts: 
	* Lead to Lead Deduplication 
	* Contact to Contact Deduplication
	* Lead to Contact Deduplication (Aiming to have it live in Q3)

**Cleaning & Enrichment Frequency:** While the email verification & enrichment jobs for net new leads, from our forms, work on a continuous bases, a full refresh, enrichment of our entire existing database of leads/contacts and the merging of duplicate leads in our database **is done on a quarterly basis**. 

