# How to update this file:
# https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#adding-features-to-webpages
# https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#creating-a-comparison-page

devops_tools:
  # gitlab_core, gitlab_starter, gitlab_premium are needed for the feature comparison
  # table under /products.
  gitlab_core:
    name: 'Core'
    short_name: 'Core'
    logo: '/images/devops-tools/ce-gitlab-logo.svg'
  gitlab_starter:
    name: 'Starter'
    short_name: 'Starter'
    logo: '/images/devops-tools/ees-gitlab-logo.svg'
  gitlab_premium:
    name: 'Premium'
    short_name: 'Premium'
    logo: '/images/devops-tools/eep-gitlab-logo.svg'
  gitlab_ultimate:
    name: 'Ultimate'
    short_name: 'Ultimate'
    logo: '/images/devops-tools/eeu-gitlab-logo.svg'
    category:
      - pipeline_authoring
      - value_stream_management
      - devops_reports
      - audit_events
      - authentication_and_authorization
      - issue_tracking
      - epics
      - backlog_management
      - source_code_management
      - code_review
      - wiki
      - design_management
      - snippets
      - web_ide
      - live_preview
      - continuous_integration
      - code_testing
      - integration_testing
      - code_quality
      - performance_testing
      - container_registry
      - package_registry
      - continuous_delivery
      - pages
      - review_apps
      - incremental_rollout
      - auto_devops
      - kubernetes_management
      - chatops
      - metrics
      - logging
      - static_application_security_testing
      - secret_detection
      - dynamic_application_security_testing
      - dependency_scanning
      - container_scanning
      - license_compliance
      - incident_management
      - status_page
      - container_host_security
      - container_network_security
      - vulnerability_management
      - static_site_editor
  gitlab_com:
    name: 'GitLab.com'
    logo: '/images/devops-tools/gitlab-logo.svg'
  gitlab_free:
    name: 'GitLab.com Free'
    logo: '/images/devops-tools/gitlab-logo.svg'
  gitlab_bronze:
    name: 'GitLab.com Bronze'
    logo: '/images/devops-tools/gitlab-logo.svg'
  gitlab_silver:
    name: 'GitLab.com Silver'
    logo: '/images/devops-tools/gitlab-logo.svg'
  gitlab_gold:
    name: 'GitLab.com Gold'
    logo: '/images/devops-tools/gitlab-logo.svg'
  github:
    name: 'GitHub'
    logo: '/images/devops-tools/github-logo.svg'
    include_file: devops-tools/github/index.html.md.erb
    category:
      - pipeline_authoring
      - audit_events
      - authentication_and_authorization
      - compliance_management
      - issue_tracking
      - boards
      - source_code_management
      - code_review
      - wiki
      - snippets
      - continuous_integration
      - container_registry
      - package_registry
      - pages
      - static_application_security_testing
      - secret_detection
      - dependency_scanning
      - vulnerability_database
  jfrog:
    name: 'JFrog'
    logo: '/images/devops-tools/jfrog-logo.png'
    include_file: devops-tools/jfrog/index.html.md.erb
    category:
      - continuous_integration
      - continuous_delivery
      - container_registry
      - package_registry
      - helm_chart_registry
      - dependency_proxy
      - static_application_security_testing
      - dependency_scanning
      - vulnerability_database
  trello:
    name: 'Trello'
    logo: '/images/devops-tools/trello-logo.svg'
    include_file: '/devops-tools/trello/index.html.md.erb'
    category:
      - issue_tracking
      - boards
    summary: |
      Trello is a web-based project management application which uses the analogy of lists and cards as a user interface. A rich API as well as email-in capability enables integration with enterprise systems, or with cloud-based integration services. Trello offers a web interface, and mobile clients, and synchronizes all of them in near real time. Trello is offered as SaaS only.

      GitLab also offers a list and card based user interface for project management, but also offers capabilities in code creation, testing, packaging, releasing, monitoring, and securing the work. While Trello does a great job of providing the single functionality of work item management and only as a SaaS solution, GitLab offers one application for the entire DevOps lifecycle, either as SaaS or in a self-managed form.
  jira:
    name: 'Jira'
    logo: '/images/devops-tools/jira-logo.png'
    include_file: devops-tools/jira/index.html.md.erb
    category:
      - boards
      - issue_tracking
      - time_tracking
  microfocus_alm_octane:
        name: 'Micro Focus ALM Octane'
        logo: '/images/devops-tools/microfocus-logo.png'
        include_file: devops-tools/microfocus-alm-octane/index.html.md
        category:
          - issue_tracking
          - epics
  gerrit:
    name: 'Gerrit'
    logo: '/images/devops-tools/gerrit-logo.png'
    include_file: '/devops-tools/gerrit/index.html.md'
    category:
      - code_review
    summary: |
        Gerrit is a free, web-based team code collaboration tool. Software developers in a team can review each other's modifications on their source code using a Web browser and approve or reject those changes. Gerrit is a fork of Rietveld, another code review tool.
  bitbucket:
    name: "Bitbucket"
    logo: '/images/devops-tools/bitbucket-logo.png'
    include_file: '/devops-tools/bitbucket/index.html.md.erb'
    category:
      - source_code_management
      - code_review
      - wiki
  jenkins:
    name: 'Jenkins'
    logo: '/images/devops-tools/jenkins-logo.svg'
    include_file: devops-tools/jenkins/index.html.md.erb
    category:
      - continuous_integration
  travis_ci:
    name: 'Travis CI'
    logo: '/images/devops-tools/travis-ci-logo.png'
    category:
     - continuous_integration
    summary: |
      Travis CI is a hosted, distributed continuous integration service used to build and test software projects hosted at GitHub. Travis CI also offers a self-hosted version called Travis CI Enterprise which requires either a GitHub Enterprise installation or account on GitHub.com. In contrast, GitLab.com and GitLab self-hosted versions offer both source code management, issue tracking, continuous integration, and many more DevOps tool chain requirements in a single application, while still also working with GitHub.

      When Travis CI has been activated for a given repository, GitHub will notify it whenever new commits are pushed to that repository or a pull request is submitted. Travis CI will then check out the relevant branch and run the commands specified in .travis.yml, which usually build the software and run any automated tests. When that process has completed, Travis notifies the developer(s) in the way it has been configured to do so.

      Although the Travis CI source is technically free software and available piecemeal on GitHub under permissive licenses, the company notes that it is unlikely that casual users could successfully integrate it on their own platforms. (ref: [wikipedia](https://en.wikipedia.org/wiki/Travis_CI)). In contrast, GitLab is open source and open core and available for everyone to contribute.
  circle_ci:
    name: 'CircleCI'
    logo: '/images/devops-tools/circle-ci-logo.svg'
    include_file: '/devops-tools/circleci/index.html.md'
    category:
     - continuous_integration
  codeship:
    name: 'CodeShip'
    logo: '/images/devops-tools/codeship-logo.png'
    include_file: '/devops-tools/codeship/index.html.md.erb'
    category:
     - continuous_integration
  bamboo:
    name: 'Bamboo'
    logo: '/images/devops-tools/bamboo-logo.png'
    category:
      - continuous_integration
    include_file: devops-tools/bamboo/index.html.md
  spinnaker:
    name: 'Spinnaker'
    logo: '/images/devops-tools/spinnaker-logo.svg'
    category:
      - continuous_delivery
    include_file: devops-tools/spinnaker/index.html.md
  urbancode:
    name: 'UrbanCode Deploy'
    logo: '/images/devops-tools/UrbanCode-logo-2019.png'
    category:
      - continuous_delivery
    include_file: devops-tools/hcl-urbancode-deploy/index.html.md
  launchdarkly:
    name: 'LaunchDarkly'
    logo: '/images/devops-tools/launchdarkly-logo.png'
    category:
      - feature_flags
    include_file: devops-tools/launchdarkly/index.html.md
  codestar:
    name: 'AWS CodeStar'
    logo: '/images/devops-tools/codestar-logo.png'
    category:
      - source_code_management
      - continuous_integration
      - continuous_delivery
    summary: 'AWS CodeStar helps you develop, build, and deploy applications on AWS by providing a pre-configured continuous delivery toolchain.  Built-in security policies for various roles secure code access while the project dashboard centrally monitors application activity. Integration with Atlassian Jira is required to create and manage Jira issues in the AWS CodeStar dashboard. GitLab is a complete DevOps platform, delivered as a single application, providing CI/CD plus issue tracking for any SLDC, including those built on AWS.'
  teamcity:
    name: 'JetBrains TeamCity'
    logo: '/images/devops-tools/teamcity.png'
    category:
      - continuous_integration
    include_file: devops-tools/teamcity/index.html.md
  new_relic:
    name: 'New Relic'
    logo: '/images/devops-tools/new-relic-logo.png'
    include_file: devops-tools/new-relic/index.html.md
    category:
      - metrics
  datadog:
    name: 'Datadog'
    logo: '/images/devops-tools/datadog-logo.png'
    include_file: devops-tools/datadog/index.html.md
    category:
      - metrics
  victorops:
    name: 'VictorOps'
    logo: '/images/devops-tools/victorops-logo.png'
    include_file: devops-tools/victorops/index.html.md
    category:
      - incident_management
  splunk:
    name: 'Splunk'
    logo: '/images/devops-tools/splunk-logo.png'
    include_file: devops-tools/splunk/index.html.md
    category:
      - metrics
      - vulnerability_management
  microfocus_APM:
    name: 'Micro Focus APM'
    logo: '/images/devops-tools/microfocus-logo.png'
    include_file: devops-tools/micro-focus-apm/index.html.md
    category:
      - metrics
  dynatrace:
    name: 'Dynatrace'
    logo: '/images/devops-tools/dynatrace-logo.svg'
    include_file: devops-tools/dynatrace/index.html.md
    category:
      - metrics
  broadcom_apm:
    name: 'Broadcom (CA Technologies)'
    logo: '/images/devops-tools/ca-agile-central.png'
    include_file: devops-tools/broadcom-apm/index.html.md
    category:
      - metrics
  ibm_apm:
    name: 'IBM APM'
    logo: '/images/devops-tools/apm-logo.png'
    include_file: devops-tools/ibm-apm/index.html.md
    category:
      - metrics
  nagios:
    name: 'Nagios XI'
    logo: '/images/devops-tools/nagios-logo.png'
    include_file: devops-tools/nagios/index.html.md
    category:
      - metrics
  gogs:
    name: 'Gogs'
    logo: '/images/devops-tools/gogs-logo.png'
    category:
      - source_code_management
    summary: |
      Gogs is a lightweight Git server written in Go which is designed to be simple to set up and operate and can be run on just about anything. It is 100% open source under the MIT OSS license and provided only in self-managed form. Gogs offers repository file viewing and editing, project issue tracking, and a built-in wiki for project documentation.
  azuremonitor:
    name: 'MS Azure Monitor'
    logo: '/images/devops-tools/azure-monitoring-logo.svg'
    include_file: devops-tools/azuremonitor/index.html.md
    category:
      - metrics
  gitea:
    name: 'Gitea'
    logo: '/images/devops-tools/gitea-logo.png'
    category:
      - source_code_management
    summary: |
      Gitea is a community fork of Gogs, a  lightweight Git server written in Go which is designed to be simple to set up and operate and can be run on just about anything. It is 100% open source under the MIT OSS license and provided only in self-managed form. Gitea offers repository file viewing and editing, project issue tracking, and a built-in wiki for project documentation.
  svn:
    name: 'SVN'
    logo: '/images/devops-tools/subversion-logo.png'
    category:
      - source_code_management
  gitlab_issue_boards:
    name: 'GitLab Issue Boards'
    logo: '/images/devops-tools/gitlab-logo.svg'
  asana:
    name: 'Asana'
    logo: '/images/devops-tools/asana-logo.png'
    include_file: devops-tools/asana/index.html.md
    category:
      - issue_tracking
      - boards
  azure_devops:
    name: 'Azure DevOps'
    logo: '/images/devops-tools/azure-devops-logo.png'
    include_file: '/devops-tools/azure_devops/index.html.md.erb'
    category:
      - issue_tracking
      - boards
      - source_code_management
      - continuous_integration
      - package_registry
      - continuous_delivery
      - wiki
      - vulnerability_management
      - container_network_security
  synopsys:
    name: 'Synopsys'
    logo: '/images/logos/synopsys-logo.png'
    include_file: devops-tools/synopsys/index.html.md
    category:
      - dependency_scanning
      - static_application_security_testing
      - license_compliance
      - container_scanning
      - dynamic_application_security_testing
  snyk:
    name: 'Snyk'
    logo: '/images/devops-tools/snyk.png'
    category:
      - dependency_scanning
      - license_compliance
      - container_scanning
    summary: |
      Snyk offers security scanning of open source components, container scanning, and license compliance.

      GitLab Ultimate offers not only these capabilities but also Static and Dynamic Application Security Testing. GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.
  ca_veracode:
    name: 'Veracode'
    logo: '/images/devops-tools/veracode-logo.png'
    include_file: '/devops-tools/veracode/index.html.md'
    category:
      - static_application_security_testing
      - secret_detection
      - dynamic_application_security_testing
  microfocus_fortify:
    name: 'MicroFocus Fortify'
    logo: '/images/devops-tools/microfocus-logo.png'
    include_file: devops-tools/microfocus-fortify/index.html.md
    category:
      - dependency_scanning
      - static_application_security_testing
      - dynamic_application_security_testing
      - vulnerability_management
  anchore:
    name: 'Anchore'
    logo: '/images/devops-tools/anchore-logo.png'
    include_file: devops-tools/anchore/index.html.md
    category:
      - dependency_scanning
      - container_scanning
  checkmarx:
    name: 'Checkmarx'
    logo: '/images/devops-tools/checkmarx-logo.png'
    category:
      - static_application_security_testing
      - dependency_scanning
    include_file: devops-tools/checkmarx/index.html.md
  hcl_appscan:
    name: 'HCL AppScan'
    logo: '/images/devops-tools/hcl_tech.png'
    category:
      - static_application_security_testing
      - dynamic_application_security_testing
      - dependency_scanning
    summary: |
      Both HCL AppScan and GitLab Ultimate offer open source component scanning along with Static and Dynamic Application Security Testing. AppScan is a mature product but, unlike GitLab Ultimate, it does not offer container scanning.

      GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, fuzz testing and license management.
  sonarqube:
    name: 'SonarQube'
    logo: '/images/devops-tools/sonarqube-logo2.png'
    category:
      - static_application_security_testing
      - secret_detection
      - dependency_scanning
    summary: |
      SonarQube is an open source tool for continuous inspection of code quality using static **software composition analysis** to detect bugs, code smells, and security vulnerabilities on 20+ programming languages. SonarQube offers reports on duplicated code, coding standards, unit tests, code coverage, code complexity, comments, bugs, and security vulnerabilities.

      GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.'
  sonatype_nexus_platform:
    name: 'Sonatype Nexus Platform'
    logo: '/images/devops-tools/sonatype-nexus-logo.png'
    category:
      - dependency_scanning
      - container_scanning
      - license_compliance
    summary: |
      Sonatype Nexus Platform is comprised of multiple products which contribute to the Sonatype Nexus security capabilities. Those products are Nexus Lifecycle, Nexus Auditor, Nexus Firewall, and Nexus Repository Pro, and the Nexus Intelligence service. In the application security space, Sonatype Nexus scans open source components for security vulnerabilities, scans containers and offers license management. Fortify relies on Sonatype for dependency scanning.

      GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.

      For packaging deployments, both Sonatype and GitLab offer container registry, but Sonatype also offers a full binary repository in the form of Nexus Repository (available in both OSS and Pro).
  sonatype_nexus_repo:
    name: 'Sonatype Nexus Repository'
    logo: '/images/devops-tools/sonatype-nexus-repo-logo.png'
    category:
      - container_registry
      - package_registry
      - helm_chart_registry
      - dependency_proxy
    summary: |
      Sonatype Nexus Repository  (available in both OSS and Pro) is a part of the Sonatype Nexus suite. Other products are Nexus Lifecycle, Nexus Auditor, Nexus Firewall, Nexus Platform, and the Nexus Intelligence service.

      Both Sonatype and GitLab offer a binary artifact repository and container registry, but Sonatype currently offers a support for more package types.
  docker_hub:
    name: 'Docker Hub'
    logo: '/images/devops-tools/docker-hub-logo.png'
    category:
      - container_registry
    summary: |
      Docker Hub is a cloud-based repository service in which Docker users and partners create, test, store and distribute container images. Docker Hub is the main public Docker repository which all docker tools go to by default. It offers both public repositories (for free) and private repositories (for a monthly recurring cost).
  docker_trusted_registry:
    name: 'Docker Trusted Registry'
    logo: '/images/devops-tools/docker-trusted-registry-logo.png'
    category:
      - container_registry
    summary: |
      Docker Trusted Registry is an enterprise-grade image storage solution from Docker. It is installed behind a firewall so that Docker images can be securely stored and managed, on-premise, letting an organization keep it's containers separate from the rest of the world. Like Docker Trusted Registry, GitLab also provides a secured container registry that can be installed behind an organizations firewalls and within it's complete control.
  aws_ecr:
    name: 'AWS Elastic Container Registry'
    logo: '/images/devops-tools/aws-ecr-logo.png'
    category:
      - container_registry
    summary: |
      Amazon Elastic Container Registry (ECR) is a fully-managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images. Amazon ECR is integrated with Amazon Elastic Container Service (ECS) but can also be used from other sources. ECR is charged by storage and by internet traffic, meaning that it's cheaper to use if you stay within the AWS bubble.

      In contrast, GitLab provides a container registry which is built-in part of the product (ie. no extra costs beyond standard operating costs).
  azure_cr:
    name: 'Azure Container Registry'
    logo: '/images/devops-tools/azure-cr-logo.png'
    category:
      - container_registry
      - helm_chart_registry
    summary: |
      Azure Container Registry (ACR) provides storage of private Docker container images, enabling fast, scalable retrieval, and network-close deployment of container workloads on Azure. Additional capabilities include geo-replication, image signing with Docker Content Trust, and Helm Chart Repositories. Azure Container Registry has tiered per day and use costs.

      In contrast, GitLab provides a container registry which is built-in part of the product (ie. no extra costs beyond standard tiered licensing costs for the single GitLab application which provides capabilities for the entire DevOps lifecycle).
  google_cr:
    name: 'Google Container Registry'
    logo: '/images/devops-tools/google-cr-logo.png'
    category:
      - container_registry
    summary: |
      Google Container Registry (GCR) provides secure, private Docker image storage on Google Cloud Platform. It provides a single place for teams to manage Docker images, perform vulnerability analysis, and decide who can access what with fine-grained access control. Existing CI/CD integrations let teams set up fully automated Docker pipelines to get fast feedback. Costs for use are based only on Google Cloud Platform storage and network usage.

      GitLab also provides a container registry which is a built-in part of the product (ie. no extra costs beyond standard tiered licensing costs for the single GitLab application which provides capabilities for the entire DevOps lifecycle).
  quay:
    name: 'RedHat Quay'
    logo: '/images/devops-tools/quay-logo.svg'
    category:
      - container_registry
      - helm_chart_registry
    summary: |
      Redhat Quay container and application registry provides secure storage, distribution, and deployment of containers on any infrastructure. It is available as an add-on for OpenShift or as a standalone component. Quay is offered both as self-managed or SaaS (quay.io).

      GitLab also provides a container registry which is a built-in part of the product (ie. no extra costs beyond standard tiered licensing costs for the single GitLab application which provides capabilities for the entire DevOps lifecycle).
  whitesource:
    name: 'WhiteSource'
    logo: '/images/devops-tools/whitesource-logo.png'
    category:
      - dependency_scanning
    summary: |
      WhiteSource scans open source code for security vulnerabilities. They claim to cover 200 programming languages. The Checkmarx dependency scanning relies on WhiteSource.

      GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.
  rapid7:
    name: 'Rapid7'
    logo: '/images/devops-tools/rapid7-logo.png'
    category:
      - dynamic_application_security_testing
      - vulnerability_management
      - container_host_security
    summary: |
      Rapid7 can crawl and assess web applications to identify vulnerabilities with offerings on-premise and in the cloud. Rapid7's InsightAppSec assesses and reports on a web application's compliance to PCI-DSS, HIPAA, OWASP Top Ten, and other regulatory requirements. Integration with Atlassian Jira gives developers full visibility within their existing workflows while the Attack Replay feature lets developers validate vulns and test source code patches on their own.

      GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management
  qualys:
    name: 'Qualys'
    logo: '/images/devops-tools/qualys-logo.png'
    category:
      - container_scanning
      - dynamic_application_security_testing
      - container_host_security
      - vulnerability_management
    summary: |
      Qualys offers cloud infrastructure security and web application security, delivered as a public or private cloud.

      GitLab Ultimate is available on-premise or in the cloud and provides integrated application security scanning with every code commit, including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.
  cloudbuild:
    name: 'Google Cloud Build'
    logo: '/images/logos/google-cloud-build.svg'
    include_file: devops-tools/cloudbuild/index.html.md
    category:
      - continuous_integration
  puppet:
    name: 'Puppet Enterprise'
    logo: '/images/logos/puppet-logo.svg'
    category:
      - kubernetes_management
      - vulnerability_management
      - static_application_security_testing
    summary: |
      Puppet is a configuration management tool and language that enables deployment and maintenance of state for large scale infrastructure. Puppet excels as managing legacy infrastructure like physical servers and VMs. Puppet was designed before widespread container adoption and does not implement Kubernetes natively.

      GitLab is a complete DevOps platform, delivered as a single application that includes not only configuration management, but also capabilities for project management, source code management, CI/CD, and monitoring. GitLab is designed for Kubernetes and cloud native applications.

      GitLab can be used together with Puppet to enable VM and bare metal configuration management. For Cloud Native applications run on Kubernetes, Puppet is not required and GitLab comes with all the functionality needed built-in.
  chef:
    name: 'Chef'
    logo: '/images/devops-tools/chef.png'
    category:
      - kubernetes_management
    summary: |
      Chef is a configuration management tool that enables deployment and maintenance of state for large scale infrastructure. Chef excels as managing legacy infrastructure like physical servers and VMs. Chef was designed before widespread container adoption and does not implement Kubernetes natively.

      GitLab is a complete DevOps platform, delivered as a single application that includes not only configuration management, but also capabilities for project management, source code management, CI/CD, and monitoring. GitLab is designed for Kubernetes and cloud native applications.

      GitLab can be used together with Chef to enable VM and bare metal configuration management. For Cloud Native applications run on Kubernetes, Chef is not required and GitLab can provide all the functionality natively.
  ansible:
    name: 'Red Hat Ansible Tower'
    logo: '/images/devops-tools/ansible-tower-logo.png'
    category:
      - kubernetes_management
    summary: |
      Ansible is an automation language and tool that can be used for configuration management and infrastructure provisioning. It enables deployment and maintenance of state for large scale infrastructure. Ansible excels as managing legacy infrastructure like physical servers and VMs. Although Ansible provides container support with Docker integration, Ansible does not implement Kubernetes natively and instead relies on a module to support Kubernetes.

      GitLab is a complete DevOps platform, delivered as a single application that includes not only configuration management, but also capabilities for project management, source code management, CI/CD, and monitoring. GitLab is designed for Kubernetes and cloud native applications.

      GitLab [can be used together with Ansible](https://blog.callr.tech/gitlab-ansible-docker-ci-cd/) to enable VM and bare metal configuration management. For Cloud Native applications run on Kubernetes, Ansible is not required and GitLab can provide all the functionality natively.
  saltstack:
    name: 'SaltStack'
    logo: '/images/devops-tools/saltstack-logo.png'
    include_file: devops-tools/SaltStack/index.html.md
    category:
      - kubernetes_management
      - license_compliance
      - vulnerability_management
      - container_host_security
      - container_network_security
  broadcom_rally:
    name: 'Broadcom Rally'
    logo: '/images/devops-tools/broadcom-logo.png'
    category:
      - authentication_and_authorization
    summary: 'Broadcom Rally is an agile project planning and management tool designed to help enterprise teams adopt and implement agile methodologies.  Based on the acquisition of Rally Software, Agile Central enables teams to manage their backlog of user stories, estimate and plan the work to deliver the user stories and then manage the actual delivery.  Rally supports multiple agile methodologies from sprints, where the delivery work is timeboxed to kanban, where the focus is on flow.'
  version_one:
    name: 'Collabnet Version One'
    logo: '/images/devops-tools/Collabnet-VersionOne.png'
    category:
      - authentication_and_authorization
    summary: 'Collabnet VersionOne is an agile project planning and management tool designed to help enterprise teams adopt and implement agile methodologies.  Based on the acquisition of VersionOne, Collabnet/VersionOne enables teams to manage their backlog of user stories, estimate and plan the work to deliver the user stories and then manage the actual delivery.  VersionOne supports Scaled Agile Framework and multiple agile methodologies from sprints,  where the delivery work is timeboxed to kanban, where the focus is on flow.'
  microfocus_ppm:
    name: 'MicroFocus PPM'
    logo: '/images/devops-tools/microfocus-logo.png'
    category:
      - authentication_and_authorization
    summary: 'MicroFocus PPM is an IT project planning and management tool designed to help enterprise teams organize, plan, prioritize and execute large enterprise projects and programs.  It facilitates capturing and prioritizing demand from business teams, allocating resources and managing execution through to completion.  It enables agile project execution through integration with MicroFocus ALM Octane and MicroFocus Agile Manager.'
  planview:
    name: 'Planview'
    logo: '/images/devops-tools/planview-logo.png'
    category:
      - epics
      - issue_tracking
      - boards
    summary: 'Planview is an IT project planning and management tool designed to help enterprise teams organize, plan, prioritize and execute large agile projects and programs. It facilitates capturing and prioritizing demand from business teams, allocating resources and managing execution through to completion.'
  jira_align:
    name: 'Jira Align'
    logo: '/images/devops-tools/atlassian-products/jira-align-logo.svg'
    category:
      - epics
      - issue_tracking
      - boards
    summary: 'Jira Align is an enterprise agile planning and management tool designed help large organizations adopt agile planning and execution.  Supporting multiple enterprise agile frameworks, AgileCraft is designed to integrate with most team level agile tools, providing enterprise portfolio planning and management capabilities.'
  jira_servicedesk:
    name: 'Jira Service Desk'
    logo: '/images/devops-tools/atlassian-products/jira-service-desk-logo.svg'
    category:
      - epics
      - issue_tracking
      - boards
    summary: 'Jira Service Desk is software built for both internal and external support. Employees and customers can submit help requests to support teams through a customer portal, via email, or through an embedded widget on your site. Support agents can then work on these requests, tracked as issues in a queue.'
  atlassian_confluence:
    name: 'Atlassian Confluence'
    logo: '/images/devops-tools/atlassian-products/confluence-logo.svg'
    category:
      - epics
      - issue_tracking
      - boards
    summary: 'Confluence is a collaboration wiki tool used to help teams to collaborate and share knowledge efficiently. With Confluence, teams capture project requirements, assign tasks to specific users, and manage several calendars at once with the help of Team Calendars add-on.'
  xebialabs:
    name: 'XebiaLabs'
    logo: '/images/devops-tools/xebialabs-logo.png'
    category:
      - continuous_delivery
    summary: |
      XebiaLabs is a platform that connects bring-your-own DevOps tools for release, deployment, and analytics. It stitches together your choice of tools "In the same way a conductor directs a symphony production, cueing all of the myriad participants to work together for a synchronized, flowing, harmonious performance, XebiaLabs orchestrates the tools in a pipeline to ensure successful, optimized release flow."

      DevOps-specific Key Performance Indicators help you identify bottlenecks and improve processes. Tune your processes for highest efficiency. Anticipate delays and take action before they turn into failures. Automate release activities to optimize delivery.

      While attempting to solve the same challenge as GitLab, XebiaLabs' approach remains expensive and complex relative to GitLab's single application across the entire SDLC.
  electric_flow:
    name: 'ElectricFlow'
    logo: '/images/devops-tools/electric_cloud-logo.png'
    include_file: '/devops-tools/electricflow/index.html.md.erb'
    category:
      - continuous_delivery
      - continuous_integration
    summary: |
      Electric Cloud ElectricFlow is a platform which provides deployment automation, release orchestration, and DevOps insights to help organizations deliver better software faster. The base platform (formerly known as Electric Commander) is used by many organizations to automate their CI/CD pipelines.

      Although Electric Cloud claims complete end to end DevOps, the platform requires a lot of integration to other tools in the tool chain in order to supplement functionality, as do just about all CI/CD point tools. In contrast, GitLab come pre-integrated with fundamental and extended functionality built-in across the DevOps lifecycle. An example is with security tools, where other CI/CD vendors such as Electric Cloud claim DevSecOps, they merely integrate to 3rd party security tools and maybe provide a dashboard. GitLab comes with many security scanning capabilities built-in
  codefresh:
    name: 'Codefresh'
    logo: '/images/devops-tools/codefresh-logo.svg'
    category:
      - continuous_integration
      - continuous_delivery
      - container_registry
    include_file: devops-tools/codefresh/index.html.md
  crucible:
    name: 'Crucible'
    logo: '/images/devops-tools/crucible-logo.png'
    include_file: '/devops-tools/crucible/index.html.md'
    category:
      - code_review
  #prisma_cloud:
    #name: 'Prisma Cloud'
    #logo: '/images/devops-tools/prisma_cloud-logo.png'
    #include_file: '/devops-tools/prisma_cloud/index.html.md'
    #category:
      #- vulnerability_management
      #- container_host_security
      #- container_network_security
      #- container_scanning
      #- compliance_management
  redmine:
    name: 'Redmine'
    logo: '/images/devops-tools/redmine-logo.png'
    include_file: '/devops-tools/redmine/index.html.md'
    category:
      - issue_tracking
  targetprocess:
    name: 'TargetProcess'
    logo: '/images/devops-tools/targetprocess.png'
    category:
      - issue_tracking
      - epics
      - boards
    summary:  |
      TargetProcess is an agile management / planning tool designed to enable visual management of projects and portfolios.  As a solution, TargetProcess provides a framework where teams can add additional feature-sets or what they call solutions to the base software such as financial management, risk management, quality management, etc.
  blueprint_storyteller:
    name: 'Blueprint Storyteller'
    logo: '/images/devops-tools/blueprint-storyteller-logo.png'
    category:
      - issue_tracking
      - epics
    summary:  |
      Blueprint Storyteller is a visual application modeling tool that enables creating process models, design artifacts, and includes workflow to manage the lifecycle events and collaboration of the artifacts.
  zoho_sprints:
    name: 'ZOHO Sprints'
    logo: '/images/devops-tools/zoho-sprints-logo.png'
    category:
      - issue_tracking
      - boards
      - epics
    summary:  |
      Zoho Sprints is a relatively new planning and tracking tool for agile teams.  Launched in October 2017, the tool brings task management, Kanban boards, epics, and other features to enable teams to organize and track their work.
  fogbugz:
    name: 'FogBugz'
    logo: '/images/devops-tools/fogbugz.png'
    category:
      - issue_tracking
      - boards
    summary: |
      FogBugz is a developer centric, project issue tracking tool that is straightforward and focused on managing the team issues.
  pivotaltracker:
    name: 'Pivotal Tracker'
    logo: '/images/devops-tools/pivotaltracker.svg'
    category:
      - issue_tracking
      - boards
    summary: |
      Pivotal Tracker is a project-planning tool that helps software development teams plan future work based on the team’s current and past performance. Tracker visualizes project work (stories) moving them through a custom  workflow, which encourages teams to decompose work items into smaller and more managable elements.  This helps to foster business discussionsa bout scope, schedule and priority.
  plutora:
    name: 'Plutora'
    logo: '/images/devops-tools/plutora-logo-2018.svg'
    category:
      - authentication_and_authorization
      - value_stream_management
    summary: |
      Plutora focuses on value stream management solutions for enterprise I. Plutora tries to improve the speed and quality of application delivery by correlating data from existing tool-chains, coordinating delivery across diverse ecosystem of development methodologies and hybrid test environments, and incorporates test metrics gathered at every step of the delivery pipeline. Essentially, it is a reporting and visibility platform across the development lifecycle.
  pluralsight_flow:
    name: 'PluralSight Flow'
    logo: '/images/devops-tools/pluralsight-logo.png'
    category:
      - authentication_and_authorization
    summary: |
      PluralSight Flow uses data from Git-based code repositories to help engineering leaders move faster, optimize work patterns, and advocate for engineering with concrete data.
  collabnet_continuum:
    name: "Collabnet Continuum"
    logo: "/images/devops-tools/digitalai-logo.png"
    category:
      - authentication_and_authorization
    summary: |
      Collabnet Continuum is an enterprise-class software planning and delivery platform that unifies Agile and DevOps lifecycle management to accelerate delivery, increase agility, and ensure alignment between business and IT.
  deployhq:
    name: 'DeployHQ'
    logo: '/images/devops-tools/deployhq.jpg'
    category:
      - continuous_delivery
  harness:
    name: 'Harness'
    logo: '/images/devops-tools/harness-logo.png'
    include_file: '/devops-tools/harness/index.html.md'
    category:
      - continuous_integration
      - continuous_delivery
  sysdig:
    name: 'SysDig'
    logo: '/images/devops-tools/sysdig-logo.svg'
    category:
      - container_host_security
      - container_network_security
      - vulnerability_management
      - license_compliance
      - container_scanning
    summary: |
      SysDig provides solutions that help secure containerized workloads through their integration with the open source Falco project.  They are capable of scanning containers for vulnerabilities and securing code in runtime through their intrusion detection and prevention capabilities.
  buildkite:
    name: 'Buildkite'
    logo: '/images/devops-tools/buildkite-logo.png'
    include_file: '/devops-tools/buildkite/index.html.md'
    category:
      - continuous_integration
