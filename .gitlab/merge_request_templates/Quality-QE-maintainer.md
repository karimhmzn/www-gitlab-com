/label ~quality ~"trainee maintainer"

<!-- Congratulations! Fill out the following MR when you feel you are ready to become -->
<!-- a quality maintainer! This MR should contain updates to a file in `data/team_members/person/` -->
<!-- declaring yourself as a maintainer of the relevant application -->

Trainee maintainer issue: <!-- Link to the trainee issue -->

### Overview

<!-- Overall experience at GitLab, how many merge requests authored, -->
<!-- gitlab-org projects at which already a maintainer -->

### Examples of reviews

<!-- Examples of reviews that hold the codebase to a high standard of quality -->

### Things to improve

<!-- Things to improve based on the feedback received during trainee maintainership -->

@gl-quality/qe-maintainers please chime in below with your thoughts, and
approve this MR if you agree.

## Developer checklist

- [ ] Before this MR is merged
  - [ ] Mention `@gl-quality/qe-maintainers`, if not done (this issue template should do this automatically)
  - [ ] Assign this issue to your manager
- [ ] After this MR is merged
  - [ ] Request a maintainer from the `#quality` Slack channel to add you as an Owner to `gl-quality/qe-maintainers`
  - [ ] Consider adding 'quality maintainer' to your [Slack notification keywords](https://slack.com/intl/en-gb/help/articles/201398467-Set-up-keyword-notifications)

## Manager checklist

- [ ] Before this MR is merged
  - [ ] The MR has been open for 3 working days
  - [ ] More than half of the existing maintainers approve the MR
  - [ ] There are no blocking concerns raised 
- [ ] After this MR is merged
  - [ ] Announce the good news in `#quality` Slack channel.

